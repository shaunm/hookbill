{{~#var 'cls' normalize=true}}
  desc
  {{#if (contains element.attrs.style 'center')}}center{{/if}}
{{/var~}}
<div class="{{vars.cls}}" {{call 'html-lang-attrs'}}>
  {{~call 'html-inline' element.children~}}
</div>
