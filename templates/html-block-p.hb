{{~#if (if_test element)~}}
{{~#var 'cls' normalize=true}}
  {{element.localname}}
  {{#if (contains element.attrs.style 'lead')}}lead{{/if}}
  {{if_class element}}
{{/var~}}
<p class="{{vars.cls}}" {{call 'html-lang-attrs'}}>
  {{~call 'html-inline' element.children~}}
</p>
{{~/if~}}
