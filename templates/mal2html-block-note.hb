{{~#if (if_test element)~}}
{{~#var 'notestyle' normalize=true}}
  {{#if (contains element.attrs.style 'advanced')}}  advanced  {{else}}
  {{#if (contains element.attrs.style 'bug')}}       bug       {{else}}
  {{#if (contains element.attrs.style 'caution')}}   caution   {{else}}
  {{#if (contains element.attrs.style 'danger')}}    danger    {{else}}
  {{#if (contains element.attrs.style 'important')}} important {{else}}
  {{#if (contains element.attrs.style 'package')}}   package   {{else}}
  {{#if (contains element.attrs.style 'plain')}}     plain     {{else}}
  {{#if (contains element.attrs.style 'sidebar')}}   sidebar   {{else}}
  {{#if (contains element.attrs.style 'tip')}}       tip       {{else}}
  {{#if (contains element.attrs.style 'warning')}}   warning   {{else}}
  note
  {{/if}}{{/if}}{{/if}}{{/if}}{{/if}}
  {{/if}}{{/if}}{{/if}}{{/if}}{{/if}}
{{/var~}}
{{~#var 'cls' normalize=true}}
  {{element.localname}}
  {{#unless (equal vars.notestyle 'note')}}note-{{vars.notestyle}}{{/unless}}
  {{if_class element}}
  {{ui_class element}}
{{/var~}}

<div class="{{vars.cls}}" {{call 'html-lang-attrs'}}>
  {{call 'mal2html-ui-data'}}
  {{#unless (contains 'plain sidebar' vars.notestyle)}}
    {{call 'icons-svg-note'}}
    {{!-- FIXME with-param vars.notestyle --}}
  {{/unless}}
  <div class="inner">
    {{call 'html-block' (get_title element)}}
    <div class="region">
      <div class="contents">
        {{call 'html-block' (get_contents element)}}
      </div>
    </div>
  </div>
</div>
{{~/if~}}

{{!-- FIXME
<!-- = note = -->
<xsl:template mode="mal2html.block.mode" match="mal:note">
  <xsl:variable name="notestyle">
    <xsl:value-of select="translate($notetitle,
                          'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
                          'abcdefghijklmnopqrstuvwxyz')"/>
  </xsl:variable>
  <div>
    <xsl:if test="$notestyle != 'plain'">
      <xsl:attribute name="title">
        <xsl:call-template name="l10n.gettext">
          <xsl:with-param name="msgid" select="$notetitle"/>
        </xsl:call-template>
      </xsl:attribute>
    </xsl:if>
    <div class="inner">
      ...
    </div>
  </div>
</xsl:if>
</xsl:template>

--}}
