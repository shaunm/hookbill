{{~#if (if_test element)~}}
{{~#var 'licls' normalize=true}}
  {{#if (is_element element 'steps' 'mallard')}}
    steps
  {{else}}
    list
  {{/if}}
{{/var~}}
{{~#var 'cls' normalize=true}}
  {{vars.licls}}
  {{if_class element}}
  {{ui_class element}}
{{/var~}}

{{#var 'el' normalize=true}}
  {{#if (is_element element 'steps' 'mallard')}}
    ol
  {{else}}
    {{#if (contains 'none box check circle diamond disc hyphen square' element.attrs.type)}}
      ul
    {{else}}
      {{#if element.attrs.type}}
        ol
      {{else}}
        ul
      {{/if}}
    {{/if}}
  {{/if}}
{{/var}}

<div class="{{vars.cls}}" {{call 'html-lang-attrs'}}>
  {{call 'mal2html-ui-data'}}
  <div class="inner">
    {{call 'html-block' (get_title element)}}
    <div class="region">
      <{{vars.el}} class="{{vars.licls}}">
        {{#each element.children}}
          {{#if (is_element this 'item' 'mallard')}}
            {{#if (if_test this)}}
              <li class="{{../vars.licls}} {{if_class this}}" {{call 'html-lang-attrs'}}>
                {{call 'html-block' (get_contents this)}}
              </li>
            {{/if}}
          {{/if}}
        {{/each}}
      </{{vars.el}}>
    </div>
  </div>
</div>
{{~/if~}}

{{!--FIXME
        <ol class="steps">
          <xsl:if test="contains(concat(' ', @style, ' '), ' continues ')">
            <xsl:attribute name="start">
              <xsl:call-template name="mal.list.start"/>
            </xsl:attribute>
          </xsl:if>
          ...
        </ol>

    <ul class="list">
          <xsl:if test="@type">
            <xsl:attribute name="style">
              <xsl:value-of select="concat('list-style-type:', @type)"/>
            </xsl:attribute>
          </xsl:if>
          <xsl:if test="contains(concat(' ', @style, ' '), ' continues ')">
            <xsl:attribute name="start">
              <xsl:call-template name="mal.list.start"/>
            </xsl:attribute>
          </xsl:if>
    </ul>

--}}
