<!doctype html>
<html>
<head>
  <meta name="viewport"
        content="width=device-width, initial-scale=1.0, user-scalable=yes"/>
  <title>
    {{~call 'html-title-text'~}}
  </title>
  {{call 'html-css'}}
  {{call 'html-js'}}
  {{call 'html-head-custom'}}
</head>
<body {{call 'html-lang-attrs'}}>
  {{call 'html-page-top-custom'}}
  <main>
    {{call 'html-page-sidebar'}}
    {{call 'html-page-sidebar-custom'}}
    <div class="page">
      <header>
        <div class="inner container">
          {{call 'html-page-header-custom'}}
          {{call 'html-page-header'}}
        </div>
      </header>
      <article>
        {{call 'html-page-body'}}
      </article>
      <footer>
        <div class="inner container">
          {{call 'html-page-footer'}}
          {{call 'html-page-footer-custom'}}
        </div>
      </footer>
    </div>
  </main>
  {{call 'html-page-bottom-custom'}}
</body>
</html>
