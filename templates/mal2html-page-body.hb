
{{!-- FIXME
{{#if self::mal:page}}
  {{call 'mal2html-editor-banner'}}
{{/if}}
--}}

{{!-- FIXME
  {{#each element.children}}
    {{#if self::mal:links[@type = 'prevnext']}}
      # apply templates to this {{call 'mal2html-links-prevnext'}}
      {{var 'hasprevnext' true}}
    {{/if}}
  {{/each}}
  {{unless vars.hasprevnext}}
    {{call 'mal2html-links-prevnext'}}
  {{/unless}}
      <xsl:apply-templates
          select="mal:links[@type = 'prevnext'][contains(concat(' ', @style, ' '), ' top ')]">
      </xsl:apply-templates>
--}}

{{!-- FIXME
<!-- page | section -->
<xsl:template match="mal:page | mal:section">
  <xsl:variable name="type" select="/mal:page/@type"/>

{{~var 'topiclinks' (get_links_topic element)~}}
{{~var 'guidelinks' (get_links_guide element)~}}
{{~var 'seealsolinks' (get_links_seealso element)~}}

  <xsl:variable name="allgroups">
    <xsl:if test="$type = 'guide'">
      <xsl:text> </xsl:text>
      <xsl:for-each select="mal:links[@type = 'topic']">
        <xsl:choose>
          <xsl:when test="@groups">
            <xsl:value-of select="@groups"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:text>#default</xsl:text>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:text> </xsl:text>
      </xsl:for-each>
    </xsl:if>
  </xsl:variable>
--}}

{{~var 'depth' (get_section_depth element)~}}
{{~var 'h1' (clamp (add 1 vars.depth) 1 6)~}}
{{~var 'h2' (clamp (add 2 vars.depth) 1 6)~}}
<div class="hgroup container">
  {{#each (get_title element)}}
    <h{{../vars.h1}}><span class="title">{{call 'html-inline' children}}</span></h{{../vars.h1}}>
  {{/each}}
  {{#each (get_subtitle element)}}
    <h{{../vars.h2}}><span class="subtitle">{{call 'html-inline' children}}</span></h{{../vars.h2}}>
  {{/each}}
</div>
<div class="region">
  <div class="contents container">
{{!-- FIXME
    {{call 'html.content.pre'}}
      FIXME? <xsl:with-param name="page" select="boolean(self::mal:page)"/>
--}}

    {{#each element.children}}
      {{#if (is_content this)}}
        {{call 'html-block'}}
      {{/if}}
    {{/each}}
  </div>
</div>

{{!-- FIXME

    <xsl:for-each
        select="*[not(self::mal:section or self::mal:title or self::mal:subtitle)]">

      <xsl:choose>
        <xsl:when test="preceding-sibling::mal:section"/>
        <xsl:when test="self::mal:links[@type = 'topic']">
          <xsl:if test="$type = 'guide'">
            <xsl:apply-templates select=".">
              <xsl:with-param name="allgroups" select="$allgroups"/>
              <xsl:with-param name="links" select="$topicnodes"/>
            </xsl:apply-templates>
          </xsl:if>
        </xsl:when>
{{#if (is 'mallard' 'links')}}
{{/if}}
{{#if (equal namespace 'http://projectmallard.org/1.0/')}}
  {{#if (equal localname 'links')}}
    {{#if (attr 'type' 'guide')}}
      {{call 'mal2html.links'}}
    {{/if}}
  {{/if}}
{{/if}}

        <xsl:when test="self::mal:links[@type = 'guide']">
          <xsl:apply-templates select=".">
            <xsl:with-param name="links" select="$guidenodes"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:when test="self::mal:links[@type = 'seealso']">
          <xsl:apply-templates select=".">
            <xsl:with-param name="links" select="$seealsonodes"/>
          </xsl:apply-templates>
        </xsl:when>
        <xsl:when test="self::mal:links[@type = 'prevnext']">
          <xsl:if test="not(contains(concat(' ', @style, ' '), ' top '))">
            <xsl:apply-templates select="."/>
          </xsl:if>
        </xsl:when>
        <xsl:when test="self::mal:links">
          <xsl:apply-templates select="."/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:apply-templates mode="mal2html.block.mode" select="."/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:for-each>
    <xsl:if test="$type = 'guide'">
      <xsl:if test="not(mal:links[@type = 'topic'])">
        <xsl:call-template name="mal2html.links.topic">
          <xsl:with-param name="links" select="$topicnodes"/>
        </xsl:call-template>
      </xsl:if>
    </xsl:if>
    <xsl:if test="$type = 'gloss:glossary'">
      <xsl:call-template name="mal2html.gloss.terms"/>
    </xsl:if>
    <xsl:call-template name="html.content.post">
      <xsl:with-param name="page" select="boolean(self::mal:page)"/>
    </xsl:call-template>
  </div>
  <xsl:for-each select="mal:section">
    <xsl:call-template name="mal2html.section"/>
  </xsl:for-each>
  <xsl:if test="self::mal:page and not(mal:links[@type = 'prevnext'])">
    <xsl:call-template name="mal2html.links.prevnext"/>
  </xsl:if>
  <xsl:variable name="postlinks" select="mal:section/following-sibling::mal:links"/>
  <xsl:if test="(not(mal:section) and (
                  ($guidenodes and not(mal:links[@type = 'guide']))
                  or
                  ($seealsonodes and not(mal:links[@type = 'seealso']))
                )) or
                ($topicnodes and $postlinks[self::mal:links[@type = 'topic']]) or
                ($guidenodes and
                  ($postlinks[self::mal:links[@type = 'guide']] or
                    (mal:section and not(mal:links[@type = 'guide'])))) or
                ($seealsonodes and
                  ($postlinks[self::mal:links[@type = 'seealso']] or
                    (mal:section and not(mal:links[@type = 'seealso']))))
                ">
    <section class="links" role="navigation">
      <div class="inner">
      <div class="hgroup pagewide"/>
      <div class="contents pagewide">
        <xsl:for-each select="$postlinks">
          <xsl:choose>
            <xsl:when test="self::mal:links[@type = 'topic']">
              <xsl:if test="$type = 'guide'">
                <xsl:apply-templates select=".">
                  <xsl:with-param name="depth" select="$depth + 1"/>
                  <xsl:with-param name="allgroups" select="$allgroups"/>
                  <xsl:with-param name="links" select="$topicnodes"/>
                </xsl:apply-templates>
              </xsl:if>
            </xsl:when>
            <xsl:when test="self::mal:links[@type = 'guide']">
              <xsl:apply-templates select=".">
                <xsl:with-param name="depth" select="$depth + 1"/>
                <xsl:with-param name="links" select="$guidenodes"/>
              </xsl:apply-templates>
            </xsl:when>
            <xsl:when test="self::mal:links[@type = 'seealso']">
              <xsl:apply-templates select=".">
                <xsl:with-param name="depth" select="$depth + 1"/>
                <xsl:with-param name="links" select="$seealsonodes"/>
              </xsl:apply-templates>
            </xsl:when>
            <xsl:when test="self::mal:links[@type = 'prevnext']">
              <xsl:if test="not(contains(concat(' ', @style, ' '), ' top '))">
                <xsl:apply-templates select=".">
                  <xsl:with-param name="depth" select="$depth + 1"/>
                </xsl:apply-templates>
              </xsl:if>
            </xsl:when>
            <xsl:otherwise>
              <xsl:apply-templates select=".">
                <xsl:with-param name="depth" select="$depth + 1"/>
              </xsl:apply-templates>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
        <xsl:if test="$guidenodes and not(mal:links[@type = 'guide'])">
          <xsl:call-template name="mal2html.links.guide">
            <xsl:with-param name="depth" select="$depth + 1"/>
            <xsl:with-param name="links" select="$guidenodes"/>
          </xsl:call-template>
        </xsl:if>
        <xsl:if test="$seealsonodes and not(mal:links[@type = 'seealso'])">
          <xsl:call-template name="mal2html.links.seealso">
            <xsl:with-param name="depth" select="$depth + 1"/>
            <xsl:with-param name="links" select="$seealsonodes"/>
          </xsl:call-template>
        </xsl:if>
      </div>
      </div>
    </section>
  </xsl:if>
  </div>
</xsl:template>
--}}
<div class="clear"></div>
