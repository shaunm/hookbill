{{~param 'color-fg' '#000000'~}}
{{~param 'color-bg' '#ffffff'~}}

{{~param 'color-red' '#c01c28'~}}
{{~param 'color-fg-red' 'rgb(173,25,36)'~}}
{{~param 'color-bg-red' 'rgb(251,243,244)'~}}

{{~param 'color-orange' '#ffa348'~}}
{{~param 'color-fg-orange' 'rgb(150,96,43)'~}}
{{~param 'color-bg-orange' 'rgb(255,243,231)'~}}

{{~param 'color-yellow' '#f8e45c'~}}
{{~param 'color-fg-yellow' 'rgb(107,99,41)'~}}
{{~param 'color-bg-yellow' 'rgb(253,251,233)'~}}

{{~param 'color-green' '#57e389'~}}
{{~param 'color-fg-green' 'rgb(46,121,73)'~}}
{{~param 'color-bg-green' 'rgb(227,250,236)'~}}

{{~param 'color-blue' '#3584e4'~}}
{{~param 'color-fg-blue' 'rgb(43,107,185)'~}}
{{~param 'color-bg-blue' 'rgb(241,246,253)'~}}

{{~param 'color-purple' '#a347ba'~}}
{{~param 'color-fg-purple' 'rgb(147,64,167)'~}}
{{~param 'color-bg-purple' 'rgb(249,243,250)'~}}

{{~param 'color-gray' '#c0bfbc'~}}
{{~param 'color-fg-gray' 'rgb(102,102,100)'~}}
{{~param 'color-bg-gray' 'rgb(244,244,244)'~}}
{{~param 'color-fg-dark' 'rgb(75,75,73)'~}}
{{~param 'color-bg-dark' 'rgb(234,234,233)'~}}

.hljs a {
  color: inherit;
  border-bottom: dotted 1px {{get_param 'color-fg-blue'}};
}
.hljs a:hover, .hljs a:hover * { color: {{get_param 'color-fg-blue'}}; }
.hljs-addition {
  color: {{get_param 'color-fg-green'}};
  background-color: {{get_param 'color-bg-green'}};
}
.hljs-deletion {
  color: {{get_param 'color-fg-red'}};
  background-color: {{get_param 'color-bg-red'}};
}
.hljs-emphasis  { font-style: italic; }
.hljs-strong    { font-weight: bold; }
.hljs-attr      { color: {{get_param 'color-fg-blue'}}; }
.hljs-attribute { color: {{get_param 'color-fg-yellow'}}; }
.hljs-built_in  { color: {{get_param 'color-fg-orange'}}; }
.hljs-bullet    { color: {{get_param 'color-fg-green'}}; }
.hljs-class     { }
.hljs-code      { color: {{get_param 'color-fg-dark'}}; }
.hljs-comment   { color: {{get_param 'color-fg-gray'}}; }
.hljs-doctag    { }
.hljs-formula   { color: {{get_param 'color-fg-dark'}}; }
.hljs-function  { }
.hljs-keyword   { color: {{get_param 'color-fg-purple'}}; }
.hljs-link      { color: {{get_param 'color-fg-orange'}}; }
.hljs-literal   { color: {{get_param 'color-fg-orange'}}; }
.hljs-meta      { color: {{get_param 'color-fg-orange'}}; }
.hljs-name      { color: {{get_param 'color-fg-red'}}; }
.hljs-number    { color: {{get_param 'color-fg-orange'}}; }
.hljs-params    { color: {{get_param 'color-fg-orange'}}; }
.hljs-quote     { color: {{get_param 'color-fg-gray'}}; }
.hljs-regexp    { color: {{get_param 'color-fg-red'}}; }
.hljs-rest_arg  { }
.hljs-section   { color: {{get_param 'color-fg-blue'}}; }
.hljs-string    { color: {{get_param 'color-fg-green'}}; }
.hljs-subst     { }
.hljs-symbol    { color: {{get_param 'color-fg-green'}}; }
.hljs-tag       { color: {{get_param 'color-fg-red'}}; }
.hljs-title     { color: {{get_param 'color-fg-blue'}}; }
.hljs-type      { }
.hljs-variable  { }
.hljs-selector-attr  { }
.hljs-selector-class { color: {{get_param 'color-fg-red'}}; }
.hljs-selector-id    { color: {{get_param 'color-fg-red'}}; }
.hljs-selector-tag   { color: {{get_param 'color-fg-purple'}}; }
.hljs-template-tag      { }
.hljs-template-variable { }
