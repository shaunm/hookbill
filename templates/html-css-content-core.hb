{{~var 'left' 'left'~}}
{{~var 'right' 'right'~}}
{{~var 'direction' 'ltr'~}}

{{~param 'html-grid-size' 1080~}}

{{~param 'color-fg' '#000000'~}}
{{~param 'color-bg' '#ffffff'~}}

{{~param 'color-red' '#c01c28'~}}
{{~param 'color-fg-red' 'rgb(173,25,36)'~}}
{{~param 'color-bg-red' 'rgb(251,243,244)'~}}

{{~param 'color-orange' '#ffa348'~}}
{{~param 'color-fg-orange' 'rgb(150,96,43)'~}}
{{~param 'color-bg-orange' 'rgb(255,243,231)'~}}

{{~param 'color-yellow' '#f8e45c'~}}
{{~param 'color-fg-yellow' 'rgb(107,99,41)'~}}
{{~param 'color-bg-yellow' 'rgb(253,251,233)'~}}

{{~param 'color-green' '#57e389'~}}
{{~param 'color-fg-green' 'rgb(46,121,73)'~}}
{{~param 'color-bg-green' 'rgb(227,250,236)'~}}

{{~param 'color-blue' '#3584e4'~}}
{{~param 'color-fg-blue' 'rgb(43,107,185)'~}}
{{~param 'color-bg-blue' 'rgb(241,246,253)'~}}

{{~param 'color-purple' '#a347ba'~}}
{{~param 'color-fg-purple' 'rgb(147,64,167)'~}}
{{~param 'color-bg-purple' 'rgb(249,243,250)'~}}

{{~param 'color-gray' '#c0bfbc'~}}
{{~param 'color-fg-gray' 'rgb(102,102,100)'~}}
{{~param 'color-bg-gray' 'rgb(244,244,244)'~}}
{{~param 'color-fg-dark' 'rgb(75,75,73)'~}}
{{~param 'color-bg-dark' 'rgb(234,234,233)'~}}

html { height: 100%; }
body {
  font-family: 'Cantarell', sans-serif;
  margin: 0; padding: 0;
  background-color: {{get_param 'color-bg'}};
  color: {{get_param 'color-fg'}};
  direction: {{vars.direction}};
}
article, aside, nav, header, footer, section {
  display: block;
  margin: 0;
  padding: 0;
}
main {
  display: flex;
  flex-flow: row;
}
main > * {
  flex: 0 0 220px;
}
main > div.page {
  flex-grow: 1;
  margin: 0;
  display: flex;
  flex-flow: column;
  align-items: stretch;
  justify-content: flex-start;
  min-height: 100vh;
}
div.page > article { flex: 1 0 auto; }
div.page > header, div.page > footer { flex: 0 1 auto; }
.container {
  max-width: {{add (get_param 'html-grid-size') -20}}px;
  margin-left: auto;
  margin-right: auto;
  padding-left: 10px;
  padding-right: 10px;
}
aside.sidebar {
  width: 300px;
  padding: 20px 10px;
  background: {{get_param 'color-bg-gray'}}
}
@media only screen and (max-width: 720px) {
  aside.sidebar {
    display: none;
  }
}
aside.sidebar-right { order: 3; }
aside.sidebar section { margin-top: 0; }
aside.sidebar * { margin-bottom: 20px; }
aside.sidebar section > div.inner > div.hgroup {
  border-bottom: none;
}
aside.sidebar section h2 {
  font-size: 1em;
  margin-bottom: 0;
}
article {
  padding-top: 10px;
  padding-bottom: 10px;
  min-height: 20em;
  background-color: {{get_param 'color-bg'}};
}
section {
  margin-top: 2.4em;
  clear: both;
}
section section {
  margin-top: 1.44em;
}
.yelp-hash-highlight {
  animation-name: yelp-hash-highlight;
  animation-duration: 0.5s;
  animation-fill-mode: forwards;
}
@keyframes yelp-hash-highlight {
  from { transform: translateY(0px) }
  25%  { transform: translateY(20px); }
  50%  { transform: translateY(0); }
  75%  { transform: translateY(10px); }
  to   { transform: translateY(0px); }
}
div.trails {
  margin: 0 -10px 0 -10px;
  padding: 0.2em 10px;
  background-color: {{get_param 'color-bg-gray'}};
}
div.trail {
  margin: 0.2em 0;
  padding: 0 1em 0 1em;
  text-indent: -1em;
  color: {{get_param 'color-fg-dark'}};
}
a.trail { white-space: nowrap; }
div.hgroup {
  margin-bottom: 0.5em;
  color: {{get_param 'color-fg-dark'}};
}
section > div.inner > div.hgroup {
  margin-top: 0;
  border-bottom: solid 1px {{get_param 'color-gray'}};
}
section.links > div.inner > div.hgroup {
  border-bottom: solid 2px {{get_param 'color-fg-blue'}};
}
section section.links > div.inner > div.hgroup {
  border: none;
}
h1, h2, h3, h4, h5, h6, h7 {
  margin: 0; padding: 0;
  font-weight: normal;
}
h1 { font-size: 2.4em; }
h2 { font-size: 1.72em; }
h3.title, h4.title, h5.title, h6.title, h7.title { font-size: 1.44em; }
h3, h4, h5, h6, h7 { font-size: 1em; }
p { line-height: 1.44em; }
div, pre, p { margin: 0; padding: 0; }
div.contents > * + *,
th > * + *, td > * + *,
dt > * + *, dd > * + *,
li > * + * { margin-top: 1em; }
p img { vertical-align: middle; }
p.lead { font-size: 1.2em; }
div.clear {
  margin: 0; padding: 0;
  height: 0; line-height: 0;
  clear: both;
}
.center { text-align: center; }

footer { background: #ddd; }
footer div.about {
  max-width: {{add (div (mul 2 (get_param 'html-grid-size')) 3) -20}}px;
  margin: 0 auto;
}
footer div.about > div.inner > div.hgroup {
  margin: 0; padding: 0;
  text-align: center;
  border: none;
}
footer div.about > div.inner > div.hgroup > h2 {
  margin: 0; padding: 0.2em;
  font-size: inherit;
}
footer div.about.ui-expander > div.inner > div.hgroup span.title:before {
  content: "";
}
div.copyrights { text-align: center; }
div.copyright { margin: 0; }
div.credits {
  display: flex;
  flex-flow: row wrap;
  align-items: stretch;
  justify-content: flex-start;
  margin: 0 -10px;
}
div.credits > * {
  vertical-align: top;
  text-align: left;
  flex: 1 0 {{add (div (get_param 'html-grid-size') 3) -60}}px;
  padding: 10px;
}
div.credits > *:empty { padding: 0; height: 0; }
ul.credits, ul.credits li {
  margin: 0; padding: 0;
  list-style-type: none;
}
ul.credits li {
  margin-{{vars.left}}: 1em;
  text-indent: -1em;
}
div.license {
  padding-bottom: 10px;
}

table {
  border-collapse: collapse;
  border-color: {{get_param 'color-gray'}};
  border-width: 1px;
}
td, th {
  padding: 0.5em;
  vertical-align: top;
  border-color: {{get_param 'color-gray'}};
  border-width: 1px;
}
thead td, thead th, tfoot td, tfoot th {
  font-weight: bold;
  color: {{get_param 'color-fg-dark'}};
  background-color: {{get_param 'color-bg-dark'}};
}
th {
  text-align: {{vars.left}};
  font-weight: bold;
  color: {{get_param 'color-fg-dark'}};
}

ul, ol, dl { margin: 0; padding: 0; }
li {
  margin: 1em 0 0 0;
  margin-{{vars.left}}: 2.4em;
  padding: 0;
}
li:first-child { margin-top: 0; }
@media (max-width: 480px) {
  li {
    margin-{{vars.left}}: 1.44em;
  }
}
dt { margin-top: 1em; }
dt:first-child { margin-top: 0; }
dt + dt { margin-top: 0; }
dd {
  margin: 0.2em 0 0 0;
  margin-{{vars.left}}: 1.44em;
}
dd + dd { margin-top: 1em; }
ol.compact li { margin-top: 0.2em; }
ul.compact li { margin-top: 0.2em; }
ol.compact li:first-child { margin-top: 0; }
ul.compact li:first-child { margin-top: 0; }
dl.compact dt { margin-top: 0.2em; }
dl.compact dt:first-child { margin-top: 0; }
dl.compact dt + dt { margin-top: 0; }

div.tiles {
  display: flex;
  flex-flow: row wrap;
  align-items: stretch;
  justify-content: stretch;
  vertical-align: top;
  clear: both;
  margin: 0 -10px;
}
div.tiles > * {
  vertical-align: top;
  margin: 0;
  padding: 10px;
  max-width: none;
}
div.tiles > *:empty { padding: 0 10px; height: 0; }
div.tile4 { flex: 1 0 {{add (div (get_param 'html-grid-size') 4) -20}}px; }
div.tile3 { flex: 1 0 {{add (div (get_param 'html-grid-size') 3) -20}}px; }
div.tile2 { flex: 1 0 {{add (div (get_param 'html-grid-size') 2) -20}}px; }
div.tile1 { flex: 1 0 {{add (get_param 'html-grid-size') -20}}px; }

a {
  text-decoration: none;
  color: {{get_param 'color-fg-blue'}};
}
a:visited { color: {{get_param 'color-fg-purple'}}; }
a:hover {
  border-bottom: dotted 1px {{get_param 'color-fg-blue'}};
}
p a {
  border-bottom: dotted 1px {{get_param 'color-fg-blue'}};
}
a img { border: none; }
