{{!--
This is called by html-page in the main element, which is a horizontal flexbox.
It is called after html-page-sidebar, which implements some stock sidebars.
All sidebars are before the page div in document order.
Right sidebars are created with flexbox child reordering.
--}}
