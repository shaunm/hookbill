{{~#if (if_test element)~}}
{{~#var 'cls' normalize=true}}
  {{element.localname}}
  {{if_class element}}
  {{ui_class element}}
{{/var~}}
<div class="{{vars.cls}}" {{call 'html-lang-attrs'}}>
{{call 'mal2html-ui-data'}}
<div class="inner">
  {{call 'html-block' (get_title element)}}
  <div class="region">
    {{call 'html-block' (get_desc element)}}
    <div class="contents">
      {{call 'html-block' (get_contents element)}}
    </div>
  </div>
</div>
</div>
{{~/if~}}
