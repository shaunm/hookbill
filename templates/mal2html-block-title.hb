{{~var 'h' (clamp (add 2 (get_section_depth element)) 1 6)~}}
{{~#var 'cls' normalize=true}}
  title title-{{element.localname}}
  {{#if (contains element.attrs.style 'heading')}}title-heading{{/if}}
  {{#if (contains element.attrs.style 'center')}}center{{/if}}
{{/var~}}
<div class="{{vars.cls}}" {{call 'html-lang-attrs'}}>
  <h{{vars.h}}><span class="title">
    {{~call 'html-inline' element.children~}}
  </span></h{{vars.h}}>
</div>
