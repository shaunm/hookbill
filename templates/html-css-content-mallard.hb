{{~var 'left' 'left'~}}
{{~var 'right' 'right'~}}

{{~param 'html-grid-size' 1080~}}

{{~param 'color-link-button-hover' 'rgb(73,144,231)'~}}

{{~param 'color-fg' '#000000'~}}
{{~param 'color-bg' '#ffffff'~}}

{{~param 'color-red' '#c01c28'~}}
{{~param 'color-fg-red' 'rgb(173,25,36)'~}}
{{~param 'color-bg-red' 'rgb(251,243,244)'~}}

{{~param 'color-orange' '#ffa348'~}}
{{~param 'color-fg-orange' 'rgb(150,96,43)'~}}
{{~param 'color-bg-orange' 'rgb(255,243,231)'~}}

{{~param 'color-yellow' '#f8e45c'~}}
{{~param 'color-fg-yellow' 'rgb(107,99,41)'~}}
{{~param 'color-bg-yellow' 'rgb(253,251,233)'~}}

{{~param 'color-green' '#57e389'~}}
{{~param 'color-fg-green' 'rgb(46,121,73)'~}}
{{~param 'color-bg-green' 'rgb(227,250,236)'~}}

{{~param 'color-blue' '#3584e4'~}}
{{~param 'color-fg-blue' 'rgb(43,107,185)'~}}
{{~param 'color-bg-blue' 'rgb(241,246,253)'~}}

{{~param 'color-purple' '#a347ba'~}}
{{~param 'color-fg-purple' 'rgb(147,64,167)'~}}
{{~param 'color-bg-purple' 'rgb(249,243,250)'~}}

{{~param 'color-gray' '#c0bfbc'~}}
{{~param 'color-fg-gray' 'rgb(102,102,100)'~}}
{{~param 'color-bg-gray' 'rgb(244,244,244)'~}}
{{~param 'color-fg-dark' 'rgb(75,75,73)'~}}
{{~param 'color-bg-dark' 'rgb(234,234,233)'~}}

div.links .desc a {
  color: inherit;
}
div.links .desc a:hover {
  color: {{get_param 'color-fg-blue'}};
}
a.bold { font-weight: bold; }

div.link-button {
  font-size: 1.2em;
  font-weight: bold;
}
.link-button a {
  display: inline-block;
  background-color: {{get_param 'color-blue'}};
  color: {{get_param 'color-bg'}};
  text-shadow: {{get_param 'color-fg-blue'}} 1px 1px 0px;
  border: solid 1px {{get_param 'color-fg-blue'}};
  padding: 0.2em 0.83em 0.2em 0.83em;
  border-radius: 4px;
}
.link-button a:visited {
  color: {{get_param 'color-bg'}};
}
.link-button a:hover {
  text-decoration: none;
  color: {{get_param 'color-bg'}};
  background-color: {{get_param 'color-link-button-hover'}};
}
div.link-button a .desc {
  display: block;
  font-weight: normal;
  font-size: 0.83em;
  color: {{get_param 'color-bg-gray'}};
}

div.floatleft {
  float: left;
  margin-right: 1em;
}
div.floatright {
  float: right;
  margin-left: 1em;
}
div.floatstart {
  float: {{vars.left}};
  margin-{{vars.right}}: 1em;
}
div.floatend {
  float: {{vars.right}};
  margin-{{vars.left}}: 1em;
}

div.title-heading h1, div.title-heading h2, div.title-heading h3,
div.title-heading h4, div.title-heading h5, div.title-heading h6 {
  font-size: 1.72em;
  font-weight: bold;
}
ul.links-heading > li { margin: 2em 0 2em 0; padding: 0; }
div.links-heading > a { font-size: 1.72em; font-weight: bold; }
ul.links-heading > li > div.desc { margin-top: 0.5em; }

div.links-uix-hover {
  position: relative;
  display: flex;
  flex-flow: row nowrap;
  align-items: stretch;
  justify-content: flex-start;
}
ul.links-uix-hover { flex-grow: 1; }
ul.links-uix-hover li { margin: 0; padding: 0; }
ul.links-uix-hover a {
  display: block;
  padding: 8px 1.2em;
  border-bottom: none;
}
ul.links-uix-hover a:hover, ul.links-uix-hover a:focus {
  background: {{get_param 'color-bg-blue'}};
}
ul.links-uix-hover img {
  display: block;
  position: absolute;
  top: 0; {{vars.left}}: 0;
  visibility: hidden;
  opacity: 0.0;
  transition: opacity 0.6s, visibility 0.6s;
}
ul.links-uix-hover a:hover img, ul.links-uix-hover a:focus img {
  visibility: visible;
  opacity: 1.0;
  transition: opacity 0.2s, visibility 0.2s;
}
@media only screen and (max-width: 480px) {
  div.links-uix-hover-img { display: none; }
  ul.links-uix-hover img { display: none; }
  ul.links-uix-hover li {
    margin-left: -10px; margin-right: -10px;
  }
  ul.links-uix-hover li a {
    padding: 10px;
  }
}

div.ui-overlay-screen {
  position: fixed;
  margin: 0;
  left: 0; top: 0;
  width: 100%; height: 100%;
  background: {{get_param 'color-fg-dark'}};
  animation-name: yelp-overlay-screen;
  animation-duration: 0.8s;
  animation-fill-mode: forwards;
}
@keyframes yelp-overlay-screen {
  from { opacity: 0.0; }
  to   { opacity: 0.6; }
}
div.ui-overlay {
  display: none;
  position: fixed;
  text-align: center;
  top: 30px;
  left: 50%;
  transform: translateX(-50%);
  z-index: 10;
}
div.ui-overlay-show {
  animation-name: yelp-overlay-slide;
  animation-duration: 0.8s;
  animation-fill-mode: forwards;
}
@keyframes yelp-overlay-slide {
  from { transform: translateY(-400px) translateX(-50%); }
  to   { transform: translateY(0) translateX(-50%); }
}
div.ui-overlay > div.inner {
  display: inline-block;
  padding: 8px;
  background-color: {{get_param 'color-bg-gray'}};
  border-radius: 4px;
  text-align: {{vars.left}};
}
div.ui-overlay img, div.ui-overlay video {
  max-height: 80vh;
  max-width: 90vw;
}
div.ui-overlay > div.inner > div.title { margin-top: -4px; }
a.ui-overlay-close {
  display: block;
  float: {{vars.right}};
  width: 30px; height: 30px;
  font-size: 22px; line-height: 30px;
  font-weight: bold;
  margin-top: -28px;
  margin-{{vars.right}}: -26px;
  padding: 1px 2px 3px 2px;
  text-align: center;
  border: none;
  border-radius: 50%;
  background-color: {{get_param 'color-blue'}};
  color: {{get_param 'color-bg'}};
}
{{!-- FIXME drop this in favor of tiles CSS in core.css --}}
div.links-tiles {
  display: flex;
  flex-flow: row wrap;
  align-items: stretch;
  justify-content: stretch;
  vertical-align: top;
  clear: both;
  margin: 0 -10px;
}
div.links-tile {
  flex: 1 0 {{add (div (get_param 'html-grid-size') 3) -20}}px;
  vertical-align: top;
  margin: 0;
  padding: 10px;
}
div.links-tiles > div.links-tile { max-width: none; }
div.links-tile:empty { padding: 0 10px; height: 0; }
div.links-tile > a {
  display: block;
  vertical-align: top;
  padding: 10px;
  border-radius: 4px;
  border: none;
}
div.links-tile > a:hover {
  background-color: {{get_param 'color-bg-blue'}};
}
div.links-tile > a > span.links-tile-img {
  display: block;
  text-align: center;
}
div.links-tile > a > span.links-tile-img > img {
  width: 100%;
  border-radius: 3px;
}
div.links-tile > a > span.links-tile-text > span.title {
  display: block;
  font-weight: bold;
}
div.links-tile > a > span.links-tile-text > * + span.title {
  margin-top: 0.5em;
}
div.links-tile > a > span.links-tile-text > span.desc {
  display: block;
  margin: 0.2em 0 0 0;
  color: {{get_param 'color-fg-dark'}};
}

a.ex-gnome-top {
  display: block;
  border: solid 1px {{get_param 'color-gray'}};
  border-radius: 4px;
  transition: box-shadow 0.5s linear;
}
a.ex-gnome-top:hover {
  box-shadow: 0px 2px 4px {{get_param 'color-gray'}};
}
span.ex-gnome-top-banner {
  display: block;
  height: 10px;
  background: {{get_param 'color-green'}};
  overflow: hidden;
}
span.ex-gnome-top-title {
  color: {{get_param 'color-fg'}};
  display: block;
  margin: 10px 20px;
  font-size: 1.2em;
}
span.ex-gnome-top-desc {
  color: {{get_param 'color-fg-gray'}};
  display: block;
  margin: 10px 20px;
}

a.ex-gnome-tile {
  display: flex;
  flex-flow: row;
  align-items: stretch;
  border: solid 1px {{get_param 'color-gray'}};
  border-radius: 4px;
  transition: box-shadow 0.5s linear;
}
a.ex-gnome-tile:hover {
  box-shadow: 0px 2px 4px {{get_param 'color-gray'}};
}
span.ex-gnome-tiles-banner {
  display: block;
  flex: 0 0 64px;
  background: {{get_param 'color-fg-blue'}};
  overflow: hidden;
}
span.ex-gnome-tiles-banner img {
  width: 64px; height: 64px;
  margin: 10px 0;
  transform: rotate(-10deg);
}
span.ex-gnome-tiles-text {
  display: block;
  margin: 10px;
}
span.ex-gnome-tiles-title {
  color: {{get_param 'color-fg'}};
  display: block;
  margin: 0;
  font-size: 1.2em;
}
span.ex-gnome-tiles-desc {
  color: {{get_param 'color-fg-gray'}};
  display: block;
  margin-top: 10px;
}

div.links-grid-container {
  margin-left: -10px;
  margin-right: -10px;
  display: flex;
  flex-flow: row wrap;
  align-items: stretch;
  justify-content: flex-start;
  vertical-align: top;
  clear: both;
}
div.links-grid {
  flex: 1 0 {{add (div (get_param 'html-grid-size') 3) -20}}px;
  padding: 10px;
}
div.links-grid:empty { padding: 0 10px; height: 0; }
div.links-grid-link { font-weight: bold; }
div.links-grid > div.desc {
  margin: 0.2em 0 0 0;
  color: {{get_param 'color-fg-dark'}};
}

div.links-divs {
  margin-left: -10px;
  margin-right: -10px;
}
a.linkdiv {
  display: block;
  margin: 0;
  padding: 10px;
  border-bottom: none;
}
a.linkdiv:hover {
  text-decoration: none;
  background-color: {{get_param 'color-bg-blue'}};
}
a.linkdiv > span.title {
  display: block;
  margin: 0;
  font-size: 1em;
  font-weight: bold;
  color: inherit;
}
a.linkdiv > span.desc {
  display: block;
  margin: 0.2em 0 0 0;
  color: {{get_param 'color-fg-dark'}};
}
span.linkdiv-dash { display: none; }
div.links-twocolumn {
  display: flex;
  flex-flow: row wrap;
  align-items: stretch;
  justify-content: flex-start;
  vertical-align: top;
  margin-left: -10px;
  margin-right: -10px;
}
div.links-twocolumn > div.links-divs {
  flex: 1 0 {{div (get_param 'html-grid-size') 3}}px;
  vertical-align: top;
  margin: 0;
}

{{!-- FIXME --}}
div.comment {
  padding: 0.5em;
  border: solid 2px {{get_param 'color-red'}};
  background-color: {{get_param 'color-bg-red'}};
}
div.comment div.comment {
  margin: 1em 1em 0 1em;
}
div.comment div.cite {
  margin: 0 0 0.5em 0;
  font-style: italic;
}

{{!-- FIXME --}}
div.tree > div.inner > div.title { margin-bottom: 0.5em; }
ul.tree {
  margin: 0; padding: 0;
  list-style-type: none;
}
li.tree { margin: -2px 0 0 0; padding: 0; }
li.tree div { margin: 0; padding: 0; }
ul.tree ul.tree {
  margin-{{vars.left}}: 1.44em;
}
div.tree-lines ul.tree { margin-left: 0; }

{{!-- FIXME --}}
span.hi {
  background-color: {{get_param 'color-bg-yellow'}};
}
span.hi.hi-ins {
  background-color: {{get_param 'color-bg-green'}};
}
span.hi.hi-del {
  background-color: {{get_param 'color-bg-red'}};
  text-decoration: line-through;
}
span.hi.hi-ins ins { text-decoration: none; }
span.hi.hi-del del { text-decoration: none; }
span.hi.hi-red { background-color: {{get_param 'color-bg-red'}}; }
span.hi.hi-orange { background-color: {{get_param 'color-bg-orange'}}; }
span.hi.hi-yellow { background-color: {{get_param 'color-bg-yellow'}}; }
span.hi.hi-green { background-color: {{get_param 'color-bg-green'}}; }
span.hi.hi-blue { background-color: {{get_param 'color-bg-blue'}}; }
span.hi.hi-purple { background-color: {{get_param 'color-bg-purple'}}; }
span.hi.hi-gray { background-color: {{get_param 'color-bg-gray'}}; }

{{!-- experimental/gloss --}}
dt.gloss-term {
  margin-top: 1.2em;
  font-weight: bold;
  color: {{get_param 'color-fg-dark'}};
}
dt.gloss-term:first-child, dt.gloss-term + dt.gloss-term { margin-top: 0; }
dt.gloss-term + dd { margin-top: 0.2em; }
dd.gloss-link {
  margin: 0 0.2em 0 0.2em;
  border-{{vars.left}}: solid 4px {{get_param 'color-blue'}};
  padding-{{vars.left}}: 1em;
}
dd.gloss-def {
  margin: 0 0.2em 1em 0.2em;
  border-{{vars.left}}: solid 4px {{get_param 'color-gray'}};
  padding-{{vars.left}}: 1em;
}
a.gloss-term {
  position: relative;
  border-bottom: dashed 1px {{get_param 'color-blue'}};
}
a.gloss-term:hover {
  text-decoration: none;
  border-bottom-style: solid;
}
span.gloss-desc {
  display: none;
  position: absolute;
  z-index: 100;
  margin: 0;
  {{vars.left}}: 0;
  top: 1.2em;
  padding: 0.2em 0.5em 0.2em 0.5em;
  min-width: 12em;
  max-width: 24em;
  overflow: hidden;
  color: {{get_param 'color-fg-dark'}};
  background-color: {{get_param 'color-bg-yellow'}};
  border: solid 1px {{get_param 'color-yellow'}};
  box-shadow: 2px 2px 4px {{get_param 'color-gray'}};
}
a.gloss-term:hover span.gloss-desc, a.gloss-term:focus span.gloss-desc {
  display: inline-block;
  animation-name: yelp-gloss-fade;
  animation-duration: 1s;
  animation-fill-mode: forwards;
}
@keyframes yelp-gloss-fade {
  from { opacity: 0.0; }
  to   { opacity: 1.0; }
}

{{!-- conditional processing --}}
.if-if { display: none; }
.if-choose, .if-when, .if-else { margin: 0; padding: 0; }
.if-choose > .if-when { display: none; }
.if-choose > .if-else { display: block; }
.if-if.if__not-target-mobile { display: block; }
.if-choose.if__not-target-mobile > .if-when { display: block; }
.if-choose.if__not-target-mobile > .if-else { display: none; }
@media only screen and (max-width: 480px) {
  .if-if.if__target-mobile { display: block; }
  .if-if.if__not-target-mobile { display: none; }
  .if-choose.if__target-mobile > .if-when { display: block; }
  .if-choose.if__target-mobile > .if-else { display: none; }
  .if-choose.if__not-target-mobile > .if-when { display: none; }
  .if-choose.if__not-target-mobile > .if-else { display: block; }
}

div.note-version {
  background-color: {{get_param 'color-bg-yellow'}};
  margin-top: 1em;
  margin-bottom: 1em;
}
span.status {
  font-size: 0.83em;
  font-weight: normal;
  padding-left: 0.2em;
  padding-right: 0.2em;
  color: {{get_param 'color-fg-dark'}};
  border: solid 1px {{get_param 'color-red'}};
  background-color: {{get_param 'color-bg-yellow'}};
}
span.status-stub, span.status-draft, span.status-incomplete, span.status-outdated {
  background-color: {{get_param 'color-bg-red'}};
}
