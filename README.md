# What the heck is this?

This is an attempt at doing documentation transforms using a template
engine that people know. I appear to be the last XSLT programmer in the
world, and sometimes people get very cross with me when I say those four
letters. So I had an idea.

What if we did it in JavaScript using Mustache templates?

Wild.

So I grabbed Handlebars, which I like a bit more than plain ol Mustache.
And then I put together this transform context that helps us track things
while we recursively process documents.

But wait. I don't have access to an XML parser. DomParser isn't in GJS.
GXml doesn't seem to be introspectable. Some day maybe I'll release Axing,
the GIO-based XML framework I've worked on. But today is not that day.

So I wrote some Python to turn XML into JSON. Like putting a skin suit
on a yak and calling it shaved.

Peace y'all.

-- Shaun
