#!/bin/env python3

import os
import sys

import lxml.etree
import json

def xml2json(node, jsonobj):
    if node.tag.startswith('{'):
        ns, ln = node.tag[1:].split('}')
        jsonobj['namespace'] = ns
        jsonobj['localname'] = ln
    else:
        jsonobj['namespace'] = None
        jsonobj['localname'] = node.tag
    jsonobj['attrs'] = {}
    for attr in node.items():
        jsonobj['attrs'][attr[0]] = attr[1]
    jsonobj['children'] = []
    if node.text is not None:
        jsonobj['children'].append(node.text)
    for child in node:
        if isinstance(child.tag, str):
            childobj = {}
            xml2json(child, childobj)
            jsonobj['children'].append(childobj)
        if child.tail is not None:
            jsonobj['children'].append(child.tail)
        

if (len(sys.argv) < 2):
    print('Usage: xml2json.py FILES...', file=sys.stderr)

for filename in sys.argv[1:]:
    basename = os.path.basename(filename)
    xml = lxml.etree.parse(filename)
    xml.xinclude()
    jsonobj = {}
    xml2json(xml.getroot(), jsonobj)
    with open(basename + '.json', 'w') as outfile:
        json.dump(jsonobj, fp=outfile, indent=True)

